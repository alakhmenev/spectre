# можно было бы фотографировать цели и отправлять отчеты об пойманных жертвах,
# гуляем так гуляем!

import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

rospy.init_node('computer_vision_sample')
bridge = CvBridge()

# ...

# Получение кадра:
def shot():
    img = bridge.imgmsg_to_cv2(rospy.wait_for_message('main_camera/image_raw', Image), 'bgr8')
