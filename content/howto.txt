git config --global user.name "Your Name"
git config --global user.email "youremail@yourdomain.com"
git config credential.helper store

git clone [link]
Склонировать репо

git pull
Получить обновлние репо

git add [file]
Добавить файлы для коммита

git commit -m 'Комментарий'
Сделать коммит

git push -all
Опубликовать коммит

Поясняю на котах: https://t.me/ithumor/6370


############################################################################

Поясняю за файловую структуру!

spectre
├── camera - ВСЕ ДЛЯ КАМЕРЫ
│   ├── report.py - сделаю потом отправку отчетов
│   └── shot.py - модуль сьемки фото, доделаю
├── config.py - конфиг
├── content - ОСТАЛЬНОЕ
│   ├── howto.txt - для тебя
│   ├── plan.txt - пишите сюда инфу по проекту
│   └── test.py - моя лаборатория
├── coordination - ВСЕ ДЛЯ КООРДИНАЦИИ И ПОЛЕТА
│   ├── back_to_home.py - будет модуль возврата домой
│   ├── land.py - я эксперементирую с посадкой, не трогать
│   └── search.py - будет поиск целей, уже знаю как сделать
├── install.py - установщик и генератор конфига, ламерам от меня подарок
├── main.py - главный файл программы
├── modules - небольшие модули
│   ├── alias.py - сокращение для частых функций
│   ├── flip.py - тестирую сальто
│   ├── pyinput.py - захват клавы
│   ├── led.py - облегченная библиотека для управления светом, на будущее
│ [RIP] lockdown.py - делаю killswitch, в процессе тк нужно эксперементировать на натуре
│   └── telemetry.py - будет собирать данные
├── neural - ТУТ ВСЯ НЕЙРОНКА
│   └── find_drone.py - будет
├── README.md - и так понятно
├── .gitignore - и так понятно
└── requirements.txt - и так понятно
