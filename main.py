import os
import sys

import config
from modules import *
from camera import *
#from coordination import *
#from neural import *

# checking the correctness of the launch
alias.cl()
alias.linux()
alias.root()

# preparation for listening
pyinut.before_listening()

# get telemetry
telemetry.battery()
telemetry.health()


####### HERE IS MAIN PROGRAM ########


# start listening
pyinput.listening()
input()
