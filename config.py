#               Here is a config for Specre
#                       Welcome!
#
# directory for photo saving
# default: "~/photo_from_spectre"
shot_dir = "~/photo_from_spectre"


# profiles
# use "home" profile for minimal movement
# default: True
home_profile = True
# maximum height (in meters!)
# default: "2"
house_height = "2"


#                     auto-lockdown
# shake
# detect shaking and auto switch off spectre
# if lockdown=True may be problems with flip and etc.
# default: True
shake = True
# shake check timeout
# default: 0.3
time_coup = 0.3

# auto-detect coup
# detect coup and auto switch off spectre
# default: True
coup = True
# coup check timeout
# default: 0.3
time_coup = 0.3


# take off from fall
# you can drop drone, and it will take off from fall
# default: False
flyfall = False


# mod key
# mod_key is used in all key bindings
# is mod_key in use?
# default: False
mod_key = False
# you can bind mod_key as "CTRL" or "ALT" etc.
# default: "<ctrl>"
what_mod_key = "<ctrl>"


# binds
# Here are the binds and modules (functions in tools module) that will be executed
# binds.keys() are the key bindings, and binds.values() are the modules to execute
# default: {"1": module1, "2": module2, "3": module3}
binds = {"1": lockdown.off, "2": land.down, "3": flip.doflip}
# here is a list of modules
#
#               telemetry
# telemetry.battery - get battery charge
# telemetry.health - check drone condition
#
#               controle
# lockdown.off - turn off the drone
# land.down - to land
# flip.doflip - do flip :/
#
#                other
# alias.cl - clear screen
# shot.saveshot - take shot from camera



# push notifications?
# default: True
push = True


# bios beep?
# emits sound with a hardware speaker
# default: True
beep = False


# battery check timeout
# default: 20
time_battery = 20
# need add response threshold
