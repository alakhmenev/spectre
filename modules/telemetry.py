# module for get telemetry
import os
import time

import config
from modules import alias


def get_battery(): # check battery power with clover lib
    # запиши результат в power
    pass


def push_battery(): # push notifications of battery power
    print("Battery charge now: " + power)
    if config.push:
        pass # уберите это
        # сейчас помру с этими кавычками, кто нибудь исправьте следующую строчку!
        #os.system('''notify-send "Battery charge now: ''' + power + '''"''' --icon=dialog-information''')
    if beep and power < 20: # разряжена на столько то процентов, к примеру
        alias.do_beep() # start beep


def check_battery(): # auto-check battery
    while True:
        time.sleep(config.time_battery)
        get_battery()

        if power < 20: # тут исправить
            print("Battery low: " + power)

            if config.push:
                pass # уберите это
                # сейчас помру с этими кавычками, кто нибудь исправьте следующую строчку!
                #os.system('''notify-send "Battery low: ''' + power + '''"''' --icon=dialog-information''')

            if config.beep:
                alias.do_beep()


def health(): # check drone
    pass


def shake(): # check shake
    pass


def coup(): # check coup
    while True:
        time.sleep(config.time_coup)

        PI_2 = math.pi / 2
        telem = get_telemetry()
        flipped = abs(telem.pitch) > PI_2 or abs(telem.roll) > PI_2

        print(flipped) # не знаю какой тут будет вывод

        # предположим что вывод будет такой:
        # up - кверху брюхом
        # down - нормальное положение

        if flipped == up:
            alias.lockdown() # bye bye
            print('LOCKDOWN! Something happened with the drone!')

            if config.push:
                os.system('''notify-send 'LOCKDOWN!' 'Something happened with the drone!' --icon=dialog-information''')

            if config.beep:
                alias.alert_beep()
