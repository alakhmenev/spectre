# most popular functions
import os
import sys
import time
from sys import platform

import config


def cl(): # clear screen
    os.system("clear")


def linux(): # fuck windows :D
    if not platform == "linux":
        input("\nNot for windows, run only on linux!\n")
        sys.exit()


def root(): # root check
    if not os.geteuid()==0:
        print("\nNeed run as root!\n")


def do_beep(): # default beep
    if config.beep == True:
        a = 1
        while a == 5:
            time.sleep(1)
            os.system("beep")
            a + 1


def alert_beep(): # alert beep
    if config.beep == True:
        a = 1
        while a == 20:
            os.system("beep")
            a + 1


def lockdown(): # turn off spectre
    from mavros_msgs.srv import CommandBool
    arming = rospy.ServiceProxy('mavros/cmd/arming', CommandBool)
    arming(False) # lockdown
