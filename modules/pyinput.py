# easy lib for pynput

import config
from pynput import keyboard


def before_listening():
    def shutdown_handler(*args): # ctrl+c react
        # Do some clean up here.
        raise SystemExit('\nExiting...')

    signal.signal(signal.SIGINT, shutdown_handler)

    def get_binds():
        mod_key_binds = {}

        for bind in config.binds:
            mod_key_binds[config.what_mod_key + '+' + bind] = config.binds.get(bind)

        return mod_key_binds


def listening():
    # old
    #hotkeys_listener = keyboard.GlobalHotKeys(get_binds())
    #hotkeys_listener.start()
    if __name__ == "__main__":
        with keyboard.GlobalHotKeys(get_binds()) as hotkeys_listener:
            hotkeys_listener.join()
