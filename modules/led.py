# easy way to controle led, here are alias for colors

import rospy
from clover.srv import SetLEDEffect

rospy.init_node('flight')
set_effect = rospy.ServiceProxy('led/set_effect', SetLEDEffect)  # define proxy to ROS-service


# colors

def red():
    set_effect(r=255, g=0, b=0)  # fill strip with red color

def green():
    set_effect(r=0, g=100, b=0)  # fill strip with green color

def blue():
    set_effect(r=0, g=0, b=255) #b=100?

def white():
    set_effect(r=255, g=255, b=255)


# random

def rainbow():
    set_effect(effect='rainbow')  # show rainbow

# хз как это выглядит, потом нужно потестить блинки и флеши
def red_flash():
    set_effect(effect='flash', r=255, g=0, b=0)  # flash twice with red color

def red_blink():
    set_effect(effect='blink', r=255, g=0, b=0)  # blink with white color

#set_effect(effect='fade', r=0, g=0, b=255)  # fade to blue color
